install(FILES
    alienelementlocalbasisfunction.hh
    analyticgridfunction.hh
    basisgridfunction.hh
    cachedcomponentwrapper.hh
    coarsegridfunctionwrapper.hh
    deformationfunction.hh
    localbasisderivativefunction.hh
    localbasisjacobianfunction.hh
    portablegreymap.hh
    portablepixelmap.hh
    vintagebasisgridfunction.hh
    virtualdifferentiablefunction.hh
    virtualgridfunction.hh
    DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dune/fufem/functions)
