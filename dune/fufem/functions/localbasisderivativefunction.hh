#ifndef LOCAL_BASIS_DERIVATIVE_FUNCTION_HH
#define LOCAL_BASIS_DERIVATIVE_FUNCTION_HH

/**
   @file localbasisderivativefunction.hh
   @brief Wrap partial derivative of local basis functions

   @author graeser@math.fu-berlin.de
 */

#include <dune/fufem/functions/localbasisjacobianfunction.hh>


/** \brief Wrap partial derivative of local basis functions
 *
 * \tparam LocalBasis The local basis type
 */
template <class FE, class Base
    = typename FE::Traits::LocalBasisType::Traits>
class LocalBasisDerivativeFunction :
    public Base
{
    public:
        typedef typename FE::Traits::LocalBasisType LocalBasis;
        typedef typename FE::Traits::LocalBasisType::Traits::DomainType DomainType;
        typedef typename FE::Traits::LocalBasisType::Traits::RangeType RangeType;

    private:
        typedef LocalBasisDerivativeFunction<FE, Base> MyType;
        typedef LocalBasisJacobianFunction<FE> JacobianFunction;

    public:

        /** \brief Construct function from local basis.
         *
         * \param localBasis Local basis
         * \param i Basis function to evaluate next
         * \param j Partial derivative to evaluate next
         */
        LocalBasisDerivativeFunction(const LocalBasis& localBasis, const int i=0, const int j=0) :
            jacobianFunction_(localBasis, i),
            j_(j)
        {}

        /** \brief Select local basis function to evaluate.
         *
         * \param i Index of the local basis function within the local basis to evaluate next.
         */
        void setIndex(int i)
        {
            jacobianFunction_.setIndex(i);
        }

        /** \brief Select component of Jacobian to evaluate.
         *
         * \param j Component of the Jacobian to evaluate next.
         */
        void setComponent(int j)
        {
            j_ = j;
        }

        /** \brief Evaluate local basis function selected last using setIndex
         *
         * \param x Local coordinates with repect to entity given in constructor.
         * \param y The result is stored here.
         */
        void evaluate(const DomainType& x, RangeType& y) const
        {
            typename JacobianFunction::RangeType D;
            jacobianFunction_.evaluate(x, D);
            y = D[0][j_];
        }

        RangeType operator()(const DomainType& x) const
        {
            return jacobianFunction_(x)[0][j_];
        }

    private:
        JacobianFunction jacobianFunction_;
        int j_;
};

#endif

