#ifndef CUBIC_TENSOR_HH
#define CUBIC_TENSOR_HH

#include "elasticitytensor.hh"

template <int dim>
class CubicTensor;

template <>
class CubicTensor<3> : public ElasticityTensor<3>
{
    public:
        CubicTensor(double C11, double C12, double C44) {

            ElasticityTensor<3>::operator=(0.0);

                (*this)[0][0] = C11;
                (*this)[1][1] = C11;
                (*this)[2][2] = C11;
                (*this)[3][3] = 2*C44;
                (*this)[4][4] = 2*C44;
                (*this)[5][5] = 2*C44;

                (*this)[0][1] = C12;
                (*this)[0][2] = C12;
                (*this)[1][0] = C12;
                (*this)[1][2] = C12;
                (*this)[2][0] = C12;
                (*this)[2][1] = C12;
        }
};

template <>
class CubicTensor<2> : public ElasticityTensor<2>
{
    public:
        CubicTensor(double C11, double C12, double C44) {

            ElasticityTensor<2>::operator=(0.0);

                (*this)[0][0] = C11;
                (*this)[1][1] = C11;
                (*this)[2][2] = 2*C44;

                (*this)[0][1] = C12;
                (*this)[1][0] = C12;
        }
};


#endif
