// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef SUBGRID_L2_FUNCTIONAL_ASSEMBLER_HH
#define SUBGRID_L2_FUNCTIONAL_ASSEMBLER_HH


#include <dune/common/function.hh>
#include <dune/common/fvector.hh>
#include <dune/istl/bvector.hh>

#include <dune/fufem/functions/virtualgridfunction.hh>
#include <dune/fufem/quadraturerules/quadraturerulecache.hh>
#include <dune/fufem/functions/basisgridfunction.hh>
#include <dune/fufem/functionspacebases/refinedp1nodalbasis.hh>


#include <dune/fufem/assemblers/localfunctionalassembler.hh>
#include <dune/fufem/assemblers/localassemblers/l2functionalassembler.hh>

#include <dune/grid/common/rangegenerators.hh>
/**  \brief Local assembler for finite element L^2-functionals on the Subgrid, given by Hostgrid-functions
  *
  *  This is needed, e.g. when assembling the right hand side of the spatial problem of a time-discretized time dependent problem with spatial adaptivity.
  *  The solution of the old time step lives on the hostgrid while the rhs is assembled on the NEW subgrid.
  */
template <class GridType, class TrialLocalFE, class T=Dune::FieldVector<double,1> >
class SubgridL2FunctionalAssembler :
    public L2FunctionalAssembler<GridType, TrialLocalFE, T>

{
    private:
        static const int dim = GridType::dimension;

        typedef typename GridType::HostGridType HostGrid;

        typedef typename GridType::template Codim<0>::Geometry::GlobalCoordinate GlobalCoordinate;
        typedef typename GridType::template Codim<0>::Geometry::LocalCoordinate LocalCoordinate;
        typedef VirtualGridFunction<GridType, T> GridFunction;
        typedef VirtualGridFunction<HostGrid, T> HostGridFunction;

        typedef BasisGridFunctionInfo<HostGrid> HostBasisGridFunctionInfo;


    public:
        typedef typename LocalFunctionalAssembler<GridType,TrialLocalFE, T>::Element Element;
        typedef typename Element::Geometry Geometry;
        typedef typename LocalFunctionalAssembler<GridType,TrialLocalFE, T>::LocalVector LocalVector;

        typedef typename GridType::HostGridType::template Codim<0>::Entity::Entity HostElement;
        typedef typename GridType::HostGridType::template Codim<0>::Entity::Geometry HostGeometry;

        typedef typename Dune::VirtualFunction<GlobalCoordinate, T> Function;
        typedef typename Function::RangeType FRangeType;

        /** \brief constructor
          *
          * \param f the (hostgrid) function representing the functional
          * \param grid the subgrid (!) type
          * \param order the quadrature order (DEFAULT 2)
          */
        [[deprecated("Please select quadrature for SubgridL2FunctionalAssembler using a QuadratureRuleKey.")]]
        SubgridL2FunctionalAssembler(const Function& f, const GridType& grid, int order=2) :
            L2FunctionalAssembler<GridType,TrialLocalFE, T>(f, order),
            quadOrder_(order),
            functionQuadKey_(dim, order),
            grid_(grid),
            f_(f),
            hostBasisGridFunctionInfo_(dynamic_cast<const HostBasisGridFunctionInfo*>(&f)),
            fAsHostGridFunction_(dynamic_cast<const HostGridFunction*>(&f))
        {}


        /** \brief constructor
         *
         * Creates a local functional assembler for an L2-functional.
         * It can assemble functionals on the subgrid given by grid
         * functions on the underlying hostgrid exactly.
         *
         * The QuadratureRuleKey given here does only specify what is
         * needed to integrate f if f is not a BasisGridFunction on
         * the hostgrid. Otherwise a quadrature rule is determined
         * automatically.
         *
         * \param f the (hostgrid) function representing the functional
         * \param grid the subgrid (!)
         * \param fQuadKey A fallback QuadratureRuleKey that specifies how to integrate f if its not a BasisGridFunction on the hostgrid
         */
        SubgridL2FunctionalAssembler(const Function& f, const GridType& grid, const QuadratureRuleKey& fQuadKey) :
            L2FunctionalAssembler<GridType,TrialLocalFE, T>(f, fQuadKey),
            quadOrder_(-1),
            functionQuadKey_(fQuadKey),
            grid_(grid),
            f_(f),
            hostBasisGridFunctionInfo_(dynamic_cast<const HostBasisGridFunctionInfo*>(&f)),
            fAsHostGridFunction_(dynamic_cast<const HostGridFunction*>(&f))
        {}

        /** \copydoc L2FunctionalAssembler::assemble
         */
        void assemble(const Element& element, LocalVector& localVector, const TrialLocalFE& tFE) const
        {
            if (not(fAsHostGridFunction_))
            {
                L2FunctionalAssembler<GridType,TrialLocalFE,T>::assemble(element, localVector, tFE);
                Dune::dwarn << "SubgridL2FunctionalAssembler fallback to L2FunctionalAssembler. Make sure the given QuadKey is appropriate." << std::endl;
                return;
            }

            typedef typename TrialLocalFE::Traits::LocalBasisType::Traits::RangeType LocalBasisRangeType;

            // get geometry and store it
            const Geometry geometry = element.geometry();

            localVector = 0.0;

            const auto baseHostElement = grid_.template getHostEntity<0>(element);

            bool trialSpaceIsRefined = IsRefinedLocalFiniteElement<TrialLocalFE>::value(tFE);

            // store values of shape functions
            std::vector<LocalBasisRangeType> values(tFE.localBasis().size());

            if (baseHostElement.isLeaf())
            {
                // get quadrature rule
                QuadratureRuleKey quadKey(tFE);
                if (quadOrder_<0)
                {
                    quadKey.setGeometryType(element.type());
                    if (hostBasisGridFunctionInfo_)
                        quadKey = quadKey.product(hostBasisGridFunctionInfo_->quadratureRuleKey(baseHostElement));
                    else
                        quadKey = quadKey.product(functionQuadKey_);
                }
                else
                {
                    bool hostGFIsRefined = hostBasisGridFunctionInfo_ and hostBasisGridFunctionInfo_->isRefinedLocalFiniteElement(baseHostElement);
                    quadKey = QuadratureRuleKey(element.type(), quadOrder_, (trialSpaceIsRefined or hostGFIsRefined));
                }
                const auto& quad = QuadratureRuleCache<double, dim>::rule(quadKey);

                // loop over quadrature points
                for (size_t pt=0; pt < quad.size(); ++pt)
                {
                    // get quadrature point
                    const LocalCoordinate& quadPos = quad[pt].position();

                    // get integration factor
                    const double integrationElement = geometry.integrationElement(quadPos);

                    // evaluate basis functions
                    tFE.localBasis().evaluateFunction(quadPos, values);

                    // compute values of function
                    FRangeType f_pos;
                    fAsHostGridFunction_->evaluateLocal(baseHostElement, quadPos, f_pos);

                    // and vector entries
                    for (size_t i=0; i<values.size(); ++i)
                    {
                        localVector[i].axpy(values[i]*quad[pt].weight()*integrationElement, f_pos);
                    }
                }

            }
            else // corresponding hostgrid element is not in hostgrid leaf
            {
                for (const auto& hostElement : descendantElements(baseHostElement, grid_.getHostGrid().maxLevel()))
                {
                    if (hostElement.isLeaf())
                    {
                        const HostGeometry hostGeometry = hostElement.geometry();

                        // get quadrature rule
                        QuadratureRuleKey quadKey(tFE);
                        if (quadOrder_<0)
                        {
                            quadKey.setGeometryType(hostElement.type());
                            if (hostBasisGridFunctionInfo_)
                                quadKey = quadKey.product(hostBasisGridFunctionInfo_->quadratureRuleKey(hostElement));
                            else
                                quadKey = quadKey.product(functionQuadKey_);
                        }
                        else
                        {
                            bool hostGFIsRefined = hostBasisGridFunctionInfo_ and hostBasisGridFunctionInfo_->isRefinedLocalFiniteElement(hostElement);
                            quadKey = QuadratureRuleKey(hostElement.type(), quadOrder_, hostGFIsRefined);
                        }
                        const auto& quad = QuadratureRuleCache<double, dim>::rule(quadKey);


                        // loop over quadrature points
                        for (size_t pt=0; pt < quad.size(); ++pt)
                        {
                            // get quadrature point
                            const LocalCoordinate& quadPos = quad[pt].position();
                            const LocalCoordinate quadPosInSubgridElement = geometry.local(hostGeometry.global(quadPos)) ;

                            // get integration factor
                            const double integrationElement = hostGeometry.integrationElement(quadPos);

                            // evaluate basis functions
                            tFE.localBasis().evaluateFunction(quadPosInSubgridElement, values);

                            // compute values of function
                            FRangeType f_pos;
                            fAsHostGridFunction_->evaluateLocal(hostElement, quadPos, f_pos);

                            // and vector entries
                            for (size_t i=0; i<values.size(); ++i)
                            {
                                localVector[i].axpy(values[i]*quad[pt].weight()*integrationElement, f_pos);
                            }
                        }
                    }
                }
            }

            return;
        }

    private:
        const int quadOrder_;
        const QuadratureRuleKey functionQuadKey_;
        const GridType& grid_;
        const Function& f_;
        const HostBasisGridFunctionInfo* hostBasisGridFunctionInfo_;
        const HostGridFunction* fAsHostGridFunction_;
};

#endif

