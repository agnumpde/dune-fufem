#include <config.h>

#include <cstdio>
//#define DUNE_FMatrix_WITH_CHECKING 1

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/istl/bvector.hh>

#include <dune/fufem/functionspacebases/conformingbasis.hh>
#include <dune/fufem/functions/basisgridfunction.hh>
#include <dune/fufem/functions/analyticgridfunction.hh>
#include <dune/fufem/functiontools/basisinterpolator.hh>

#include <dune/fufem/functionspacebases/dunefunctionsbasis.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>

#include "common.hh"


template<class DT, class RT>
class F : public VirtualDifferentiableFunction<DT, RT>
{
    public:
        typedef typename VirtualDifferentiableFunction<DT, RT>::DerivativeType DerivativeType;

        virtual void evaluate(const DT& x, RT& y) const
        {
            for (int i=0; i<y.dimension; ++i)
            {
                y[i] = 0.0;
                for (int j=0; j<x.dimension; ++j)
                    y[i] += (i+1)*j*x[j];
            }
        }

        virtual void evaluateDerivative(const DT& x, DerivativeType& d) const
        {
            for (std::size_t i=0; i<d.N(); ++i)
                for (std::size_t j=0; j<d.M(); ++j)
                    d[i][j] = (i+1)*j;
        }
};



/** \brief Test suite for BasisGridFunction
 *
 * Run a few standard tests for BasisGridFunction
 */
template <class CoeffType>
struct BasisGridFunctionTestSuite
{
    template <class GridType>
    bool check(const GridType& grid)
    {
        bool passed = true;

        typedef typename GridType::LeafGridView GridView;
        GridView gridView = grid.leafGridView();

        // Test with a P1 basis
        {
            typedef DuneFunctionsBasis<Dune::Functions::LagrangeBasis<GridView, 1> > P1Basis;
            typedef ConformingBasis<P1Basis> ConformingP1Basis;

            P1Basis p1Basis(gridView);
            if (not(checkWithGridViewAndBasis(gridView, p1Basis)))
            {
                std::cout << "Check failed with P1Basis" << std::endl;
                passed = false;
            }

            ConformingP1Basis conformingP1Basis(p1Basis);
            if (not(checkWithGridViewAndBasis(gridView, conformingP1Basis)))
            {
                std::cout << "Check failed with ConformingBasis<P1Basis>" << std::endl;
                passed = false;
            }
        }

        return passed;
    }

    template <class GridView, class Basis>
    bool checkWithGridViewAndBasis(const GridView& gridView, const Basis& basis)
    {
        bool passed = true;

        typedef typename GridView::template Codim<0>::Geometry::GlobalCoordinate GlobalCoordinate;
        typedef typename CoeffType::value_type RangeType;


        typedef F<GlobalCoordinate, RangeType> TestF;
        typedef AnalyticGridFunction<typename GridView::Grid, TestF> TestFAsGF;

        typedef BasisGridFunction<Basis, CoeffType> TestBasisGF;

        TestF f;
        TestFAsGF fAsGF(gridView.grid(), f);

        CoeffType coefficients;
        Functions::interpolate(basis, coefficients, f);

        TestBasisGF testBasisGF(basis, coefficients);

        passed = passed and compareEvaluateByGridViewQuadrature(fAsGF, testBasisGF, gridView, 7);
        passed = passed and compareEvaluateDerivativeByGridViewQuadrature(fAsGF, testBasisGF, gridView, 7);
        passed = passed and compareEvaluateLocalByGridViewQuadrature(fAsGF, testBasisGF, gridView, 7);
        passed = passed and compareEvaluateDerivativeLocalByGridViewQuadrature(fAsGF, testBasisGF, gridView, 7);

        return passed;
    }

};

int main(int argc, char** argv)
{
    Dune::MPIHelper::instance(argc, argv);

    std::cout << "This is the BasisGridFunctionTest" << std::endl;

    bool passed = true;

    static const int block_size_1 = 1;
    static const int block_size_2 = 2;
//    static const int block_size_3 = 3;

    {
        typedef Dune::BlockVector<Dune::FieldVector<double, block_size_1> > CoeffType;
        BasisGridFunctionTestSuite<CoeffType> tests;
        passed = passed and checkWithStandardAdaptiveGrids(tests);
    }
    {
        typedef Dune::BlockVector<Dune::FieldVector<double, block_size_2> > CoeffType;
        BasisGridFunctionTestSuite<CoeffType> tests;
        passed = passed and checkWithStandardAdaptiveGrids(tests);
    }
//    {
//        typedef std::vector<Dune::FieldMatrix<double, block_size_2, block_size_3> > CoeffType;
//        BasisGridFunctionTestSuite<CoeffType> tests;
//        passed = checkWithStandardAdaptiveGrids(tests);
//    }

    return passed ? 0 : 1;
}
