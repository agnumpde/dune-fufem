// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_FUFEM_BOUNDARY_PATCH_PROLONGATOR_HH
#define DUNE_FUFEM_BOUNDARY_PATCH_PROLONGATOR_HH

/**
 * \file
 * \brief Contains methods which prolong coarse a boundary patch to finer ones.
 */

#include <vector>



#include <dune/common/exceptions.hh>
#include <dune/fufem/boundarypatch.hh>
#include <dune/fufem/facehierarchy.hh>



/**
 * \brief Contains methods which prolong coarse a boundary patch to finer ones.
 */
template <class GridType>
class BoundaryPatchProlongator {
public:

    /**
     * \brief Prolong coarse BoundaryPatch to all levels
     *
     * This method prolongs a BoundaryPatch given on level 0 to BoundaryPatches
     * for finer levels.
     *
     * \param patches A vector of BoundaryPatches for LevelGridViews.
     *                It must contain at least one entry.
     *                patches[0] must be an initialized on level 0.
     */
    static void prolong(std::vector<BoundaryPatch<typename GridType::LevelGridView> >& patches)
    {
        std::vector<BoundaryPatch<typename GridType::LevelGridView>* > patchPointers;
        patchPointers.resize(patches.size());
        for(size_t i=0; i<patches.size(); ++i)
            patchPointers[i] = &(patches[i]);
        prolong(patchPointers);
    }

    /**
     * \brief Prolong coarse BoundaryPatch to all levels
     *
     * This method prolongs a BoundaryPatch given on level 0 to BoundaryPatches
     * for finer levels. The BoundaryPatches are given as vector of smart/raw pointers.
     *
     * \param patches A vector of pointers to BoundaryPatches for LevelGridViews.
     *                It must contain at least one entry.
     *                *(patches[0]) must be an initialized on level 0.
     */
    template<class BoundaryPatchPointerType>
    static void prolong(std::vector<BoundaryPatchPointerType>& patches)
    {
        typedef typename GridType::template Codim<0>::Entity::Entity Element;

        if (not(patches[0]->isInitialized()))
            DUNE_THROW(Dune::Exception, "Coarse boundary patch has not been set up correctly!");

        const GridType& grid = patches[0]->gridView().grid();

        int maxLevel = std::min((size_t)grid.maxLevel(), patches.size()-1);
        // Set array sizes correctly
        for (int i=1; i<=maxLevel; i++)
            patches[i]->setup(grid.levelGridView(i));

        for (const auto& pIt : *patches[0])
        {
            const Element& inside = pIt.inside();

            if (not(inside.isLeaf()) and (inside.level()<maxLevel))
            {
                Face<GridType> face(grid, inside, pIt.indexInInside());

                typename Face<GridType>::HierarchicIterator it = face.hbegin(maxLevel);
                typename Face<GridType>::HierarchicIterator end = face.hend(maxLevel);

                for(; it!=end; ++it)
                    patches[it->level()]->addFace(it->element(), it->index());
            }
        }
    }


    /**
     * \brief Prolong coarse BoundaryPatch to leaf level
     *
     * This method prolongs a BoundaryPatch given on some level to a BoundaryPatch
     * on the leaf view.
     *
     * \param coarsePatch The coarse BoundaryPatch given on some LevelGridView
     * \param finePatch The fine BoundaryPatch on the LeafGridView
     */
    static void prolong(const BoundaryPatch<typename GridType::LevelGridView>& coarsePatch,
                        BoundaryPatch<typename GridType::LeafGridView>& finePatch)
    {

        if (not(coarsePatch.isInitialized()))
            DUNE_THROW(Dune::Exception, "Coarse boundary patch has not been set up correctly!");

        const GridType& grid = coarsePatch.gridView().grid();

        // Set array sizes correctly
        finePatch.setup(grid.leafGridView());

        for (const auto& pIt : coarsePatch)
        {
            const auto& inside = pIt.inside();

            if (inside.isLeaf())
                finePatch.addFace(inside, pIt.indexInInside());
            else
            {
                Face<GridType> face(grid, inside, pIt.indexInInside());

                typename Face<GridType>::HierarchicIterator it = face.hbegin(grid.maxLevel());
                typename Face<GridType>::HierarchicIterator end = face.hend(grid.maxLevel());

                for(; it!=end; ++it)
                {
                    if (it->element().isLeaf())
                        finePatch.addFace(it->element(), it->index());
                }
            }
        }
    }

};


#endif

