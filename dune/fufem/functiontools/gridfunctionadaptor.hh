#ifndef GRID_FUNCTION_ADAPTOR_HH
#define GRID_FUNCTION_ADAPTOR_HH

#include <memory>

#include <dune/common/fvector.hh>
#include <dune/common/shared_ptr.hh>

#include <dune/istl/bvector.hh>

#include <dune/fufem/functiontools/basisinterpolator.hh>
#include <dune/fufem/functiontools/basisidmapper.hh>

/**
 * \brief This class provides the functionality to interpolate grid functions
 * after grid refinement.
 *
 * To interpolate functions given by coefficient vectors u,w with respect
 * to a basis b of type B do the following:
 *
 * \code
 * GridFunctionAdaptor<B> adaptor(b, allowVanishingElements);
 *
 * // [... refine grid ...]
 *
 * b.update();
 *
 * adaptor.adapt(u);
 * adaptor.adapt(w);
 * \endcode
 *
 * If you want to reuse the GridFunctionAdaptor for further refinements you can continue
 *
 * \code
 * // [... computations on refined grid ...]
 *
 * adaptor.update();
 *
 * // [... refine grid ...]
 *
 * b.update();
 *
 * adaptor.adapt(u);
 * adaptor.adapt(w);
 * \endcode
 *
 * \tparam B Type of basis.
 */
template<class B>
class GridFunctionAdaptor
{
    public:
        typedef B Basis;
        typedef typename Basis::ReturnType RT;


        /**
         * \brief Create grid function adaptor
         */
        GridFunctionAdaptor() :
            basisCopied_(false),
            basis_(),
            idMapper_(),
            oldBasis_(0)
        {}


        /**
         * \brief Create grid function adaptor
         *
         * By default a copy of the basis is stored internally.
         * This is done to ensure that adaptation works properly
         * if the basis handed to the constructor is updated after
         * refinement and used in the adapt method.
         *
         * \param basis Global function space basis before refinement
         * \param allowVanishingElements Flag to control if elements might vanish. If enabled all ids of ancestor elements are stored.
         * \param copyBasis Flag to control if basis is copied internally(default:yes)
         */
        GridFunctionAdaptor(const B& basis, bool allowVanishingElements = false, bool copyBasis=true) :
            basis_(),
            idMapper_(),
            oldBasis_(0),
            basisCopied_(false),
            allowVanishingElements_(allowVanishingElements)
        {
            update(basis, copyBasis);
        }


        /**
         * \brief Update grid function adaptor
         *
         * This can be called BEFORE the grid is adapted if you
         * want to reuse an earlier created GridFunctionAdaptor.
         *
         * Don't call this AFTER grid.adapt() and BEFORE gridfunctionadaptor.adapt(xyz) !
         *
         * By default a copy of the basis is stored internally.
         * This is done to ensure that adaptation works properly
         * if the basis handed to the update method is updated after
         * refinement and used in the adapt method.
         *
         * \param basis Global function space basis before refinement
         * \param copyBasis Flag to control if basis is copied internally(default:yes)
         */
        void update(const B& basis, bool copyBasis=true)
        {
            if (copyBasis)
                basis_ = std::make_shared<Basis>(basis);
            else
                basis_ = Dune::stackobject_to_shared_ptr(basis);

            idMapper_ = std::make_shared<const BasisIdMapper<B> > (*basis_, allowVanishingElements_);

            oldBasis_ = &basis;
            basisCopied_ = copyBasis;
        }


        /**
         * \brief Update grid function adaptor using the same basis
         *
         * This can be called BEFORE the grid is adapted if you
         * want to reuse an earlier created GridFunctionAdaptor.
         *
         * Don't call this AFTER grid.adapt() and BEFORE gridfunctionadaptor.adapt(xyz) !
         *
         * This method uses the basis and the same flag copyBasis that you specified
         * in the constructor or the last call to update(basis,copyBasis).
         */
        void update()
        {
            update(*oldBasis_, basisCopied_);
        }


        virtual ~GridFunctionAdaptor()
        {}


        /** \brief Adapt function
         *
         * Adapts the function given as coefficient related to the old basis
         * handed to the constructor to the new basis given to this method.
         *
         * \param toV Coefficient vector with respect to the target basis
         * \param toBasis Target basis to be used for the adapted function.
         * \param fromV Coefficient vector with respect to the basis handed to the constructor
         */
        template<class ToBasis, class C>
        void adapt(C& toV, const ToBasis& toBasis, const C& fromV) const
        {
            Functions::interpolate(toBasis, toV, idMapper_->makeFunction(toBasis.getGridView(), fromV));
        }


        /** \brief Adapt function
         *
         * Adapts the function given as coefficient related to the old basis
         * handed to the constructor to the new basis given to this method.
         * The result is stored in the same vector.
         *
         * \param v Coefficient vector with respect to the basis handed to the constructor
         * \param toBasis Target basis to be used for the adapted function.
         */
        template<class ToBasis, class C>
        void adapt(C& v, const ToBasis& toBasis) const
        {
            C fromV = v;
            adapt(v, toBasis, fromV);
        }


        /** \brief Adapt function
         *
         * Adapts the function given as coefficient related to the old basis
         * handed to the constructor to the same basis which must be updated
         * before call to this method.
         * The result is stored in the same vector.
         *
         * \param v Coefficient vector with respect to the basis handed to the constructor
         */
        template<class C>
        void adapt(C& v) const
        {
            adapt(v, *oldBasis_);
        }



    private:

        std::shared_ptr<const Basis> basis_;
        std::shared_ptr<const BasisIdMapper<B> > idMapper_;

        const Basis* oldBasis_;

        bool basisCopied_;
        bool allowVanishingElements_;

};




#endif

