// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_FUFEM_PYTHON_MODULE_HH
#define DUNE_FUFEM_PYTHON_MODULE_HH

// Only introduce the dune-python interface if python
// was found and enabled.
#if HAVE_PYTHON || DOXYGEN

#include <Python.h>

#include <string>
#include <sstream>
#include <vector>
#include <map>

#include <dune/common/exceptions.hh>
#include <dune/common/shared_ptr.hh>
#include <dune/common/fvector.hh>
#include <dune/common/parametertree.hh>
#include <dune/common/typetraits.hh>
#include <dune/common/stringutility.hh>

#include <dune/grid/common/gridfactory.hh>

#include <dune/fufem/python/reference.hh>


namespace Python
{

// forward declarations
void handlePythonError(const std::string&, const std::string&);
void run(const std::string& code);



/**
 * \brief Wrapper class for python modules
 *
 * This class derives from Python::Reference and
 * encapsulates module related functions.
 */
class Module :
    public Reference
{
    public:

        /**
         * \brief A stream that executed all python code it is feeded with on destruction
         *
         * This class is not inteded to be used directly.
         * It is only a helper class for the runStream() function.
         */
        class AutoRunCodeStream :
            public std::stringstream
        {
            public:
                /**
                 * \brief Destructor
                 *
                 * This will execute the code feeded to the stream
                 * at once.
                 */
                ~AutoRunCodeStream()
                {
                    if (module_)
                        Module(module_).run(this->str());
                    else
                        Python::run(this->str());
                }

                /**
                 * \brief Default constructor
                 */
                AutoRunCodeStream()
                {}

                /**
                 * \brief Default constructor
                 */
                AutoRunCodeStream(Reference& module) :
                    module_(module)
                {}

                /**
                 * \brief Copy constructor
                 *
                 * This will not copy the code feeded to the copied object
                 * to the newly created one in order to avoid unintended
                 * multiple execution.
                 */
                AutoRunCodeStream(const AutoRunCodeStream& other) :
                    module_(other.module_)
                {}

            private:
                Reference module_;
        };



    public:

        /**
         * \brief Construct empty Module
         */
        Module() :
            Reference()
        {}

        /**
         * \brief Construct Module from PyObject*
         *
         * Only to be used if you want to extend the interface
         * using the python api.
         *
         * This forwards to the corresponding constructor of Reference
         * and then checks if the python object is a module. If this is
         * not the case an exception is thrown. As a consequence the
         * reference count of the PyObject will be correctly decreased by
         * ~Reference even if the exception is thrown.
         *
         * But be carefull to always increment the count of a borrowed
         * reference BEFORE calling this constructor. I.e. always use
         *
         *   Module(Imp::inc(p))
         *
         * instead of
         *
         *   Imp::inc(Module(p))
         *
         * The latter may throw an exception and then decrease the count
         * before it is increased.
         *
         */
        Module(PyObject* p) :
            Reference(p)
        {
            assertModule(p_, "Module(PyObject*)");
            if (p_)
                dict_ = get("__dict__");
        }

        /**
         * \brief Construct Module from Reference
         *
         * This checks if the python object is a module. If this is
         * not the case an exception is thrown.
         *
         * For implementors:
         *
         * This will increment the count for the
         * stored reference.
         */
        Module(const Reference& other) :
            Reference(other)
        {
            assertModule(p_, "Module(Reference&)");
            if (p_)
                dict_ = get("__dict__");
        }

        /**
         * \brief Destructor
         */
        virtual ~Module()
        {}

        /**
         * \brief Assignment
         *
         * This will checks if the python object is a module.
         * If this is not the case an exception is thrown.
         */
        virtual Module& operator= (const Reference& other)
        {
            assertModule(other, "Module::operator=(Reference&)");
            Reference::operator=(other);
            if (p_)
                dict_ = get("__dict__");
            return *this;
        }

        /**
         * \brief Run python code given as string
         *
         * The code is executed  in the context of this module.
         * Hence all names are resolved or created in this module.
         *
         * \param code A string containing python code
         */
        virtual void run(const std::string& code)
        {
            assertPyObject("Module::run()");
            if (not PyRun_String(code.c_str(), Py_file_input, dict_, dict_))
                handlePythonError("Module::run()", Dune::formatString("failed to execute the following code '%s'", code.c_str()));
        }

        /**
         * \brief Obtain a stream to feed the code with multiple lines of python code
         *
         * The code is executed in the context of
         * this module. Hence all names are resolved
         * or created in this module.
         *
         * \returns A stream that will execute all feeded code on destruction
         */
        AutoRunCodeStream runStream()
        {
            assertPyObject("Module::runStream()");
            return AutoRunCodeStream(*this);
        }

        /**
         * \brief Run python code in file given by name
         *
         * The code is executed in the context of
         * this module. Hence all names are resolved
         * or created in this module.
         *
         * \param fileName A string containing the file name
         */
        void runFile(const std::string& fileName)
        {
            assertPyObject("Module::runFile()");
            // the last argument specifies that the file should be closed
            // after executing the code
            PyRun_FileEx(fopen(fileName.c_str(), "r"), fileName.c_str(), Py_file_input, dict_, dict_, 1);
        }

        /**
         * \brief Evaluate python expression given as string
         *
         * This evaluates a python expressions in the given string
         * in the context of this module.
         *
         * \param expression A string containing a python expression 
         * \returns Result of expression evaluation
         */
        Reference evaluate(const std::string& expression)
        {
            return PyRun_String(expression.c_str(), Py_eval_input, dict_, dict_);
        }


        /**
         * \brief Import another module into this one
         *
         * This import the given module under given importedName
         * into this module.
         *
         * \param module The module to import
         * \param importedName The module will be imported under this name
         * \returns Reference to imported module
         */
        Module importAs(Module module, const std::string& importedName)
        {
            if (not module)
                handlePythonError("Module::importAs(Module,string)", "failed to import module");
            set(importedName, module);
            return module;
        }

        /**
         * \brief Import another module into this one
         *
         * This import the module with given moduleName under given importedName
         * into this module.
         *
         * \param moduleName Name of module to import
         * \param importedName The module will be imported under this name
         * \returns Reference to imported module
         */
        Module importAs(const std::string& moduleName, const std::string& importedName)
        {
            Module module(PyImport_ImportModule(moduleName.c_str()));
            return importAs(module, importedName);
        }

        /**
         * \brief Import another module into this one
         *
         * This imports the given module under its own name
         * into this module.
         *
         * \param module The module to import
         * \returns Reference to imported module
         */
        Module import(Module module)
        {
            if (not module)
                handlePythonError("Module::import(Module)", "failed to import module");
            return importAs(module, module.get("__name__").str());
        }

        /**
         * \brief Import another module into this one
         *
         * This import the module with given moduleName under
         * its own name into this module.
         *
         * \param moduleName Name of module to import
         * \returns Reference to imported module
         */
        Module import(const std::string& moduleName)
        {
            return importAs(moduleName, moduleName);
        }

    protected:

        /**
         * \brief Assert that internal PyObject* is not NULL and module and raise exception otherwise
         *
         * \param origin A string describing the origin of the error
         */
        static void assertModule(PyObject* p, const std::string& origin)
        {
            if (p and (not PyModule_Check(p)))
                DUNE_THROW(Dune::Exception,
                    "Python error occured." << std::endl <<
                    "  Origin:  " << origin << std::endl <<
                    "  Error:   Trying to use a non-module as module");
        }

        Reference dict_;
};


} // end of namespace Python



#else
    #warning dunepython.hh was included but python was not found or enabled!
#endif // DUNE_FUFEM_PYTHON_MODULE_HH


#endif

