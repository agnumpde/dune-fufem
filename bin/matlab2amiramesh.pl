#!/usr/bin/perl -w

if (@ARGV!=3) {
  die "Usage: matlab2amiramesh <vertexfile> <trianglefile> <outputfile>\n";
}

$pointfilename = $ARGV[0];
$trifilename   = $ARGV[1];
$outfilename   = $ARGV[2];

###################################################
#  Parse the file containing the vertex coordinates
###################################################

unless (open POINTFILE, "$pointfilename") {
  die "Couldn't open $pointfilename for reading!\n";
}

$nVertices = 0;
while (<POINTFILE>) {

  $nVertices = $nVertices + 1;
}

close (POINTFILE);


###################################################
#  Parse the file containing the triangles
###################################################

unless (open TRIANGLEFILE, "$trifilename") {
  die "Couldn't open $trifilename for reading!\n";
}


$nTriangles = 0;
while (<TRIANGLEFILE>) {
 # print ($_);
  $nTriangles = $nTriangles + 1;
}

close (TRIANGLEFILE);


###################################################
#  Write the AmiraMesh file
###################################################

unless (open OUTFILE, ">$outfilename") {
  die "Couldn't open $outfilename for writing!\n";
}

print OUTFILE "# AmiraMesh 2D ASCII 2.0\n";
print OUTFILE "# CreationDate: 7.12.2001  13.41.34\n";
print OUTFILE "\n";

print OUTFILE "define Nodes       $nVertices\n";
print OUTFILE "define Triangles   $nTriangles\n";
print OUTFILE "\n";

print OUTFILE "Parameters {\n";
print OUTFILE '    ContentType "HxTriangularGrid"';
print OUTFILE "\n";
print OUTFILE "  }\n";
print OUTFILE "\n";

print OUTFILE "Materials {\n";
print OUTFILE '            { Name "Substrat1"';
print OUTFILE "\n";
print OUTFILE "              Color 0.8 0.1 0.1\n";
print OUTFILE "            }\n";
print OUTFILE "          }\n";
print OUTFILE "\n";

print OUTFILE " Nodes { float [2] Coordinates } = \@1\n";
print OUTFILE " Triangles { int[3] Nodes } = \@2\n";
print OUTFILE " Triangles { byte Materials } = \@3\n";
print OUTFILE "\n";

print OUTFILE " # Punktkoordinaten\n";
print OUTFILE "\@1\n";

#  Write the vertex coordinates
unless (open POINTFILE, "$pointfilename") {
  die "Couldn't open $pointfilename for reading!\n";
}

while (<POINTFILE>) {
  print OUTFILE "$_";
}

close (POINTFILE);
print OUTFILE "\n\n";

#  Write the triangle indices
print OUTFILE " # Dreiecksindizes\n";
print OUTFILE "\@2\n";

unless (open TRIANGLEFILE, "$trifilename") {
  die "Couldn't open $trifilename for reading!\n";
}

while (<TRIANGLEFILE>) {
  if (/(\S+)\s+(\S+)\s+(\S+)\s+(\S+)/) {
    printf OUTFILE "%d %d %d\n", $1, $2, $3;
  }

}

close (TRIANGLEFILE);

#  Write the material indices
print OUTFILE " # Materialindizes pro Dreieck\n";
print OUTFILE "\@3\n";

unless (open TRIANGLEFILE, "$trifilename") {
  die "Couldn't open $trifilename for reading!\n";
}

while (<TRIANGLEFILE>) {
  if (/(\S+)\s+(\S+)\s+(\S+)\s+(\S+)/) {
    printf OUTFILE "%d\n", $4;
  }

}

close (TRIANGLEFILE);

close (OUTFILE);
