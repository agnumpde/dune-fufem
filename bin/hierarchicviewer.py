import threading
import sys
import time
import qt
import getopt

from regexptools import *
from iteratortools import *
from treetools import *


class HListView(qt.QListView):

    def __init__(self, follow, *args):
        apply(qt.QListView.__init__, (self,) + args)
        self.addColumn("item")
        self.header().hide()
        self.setRootIsDecorated(1)
        self.setSorting(-1)
        self.setFont(qt.QFont('monospace',8))

        self.items=[]
        self.lastAppendedStack=[self]

        self.buffer=[]

        self.maxLevel=0
        self.follow=follow

    def queueItem(self,item):
        self.buffer.append(item)

    def setMaxLevel(self, maxLevel):
        self.maxLevel = maxLevel
        for item in self.items:
            if (item[1]<self.maxLevel):
                item[2].setOpen(True)
            else:
                item[2].setOpen(False)

    def toggleFollow(self, *args):
        if (len(args)>0):
            self.follow = args[0]
        else:
            self.follow = not(self.follow)

    def customEvent(self,event):
        self.processQueue()

    def processQueue(self):
        for item in self.buffer:
            level = item[1]
            stackLevel = len(self.lastAppendedStack)-2
            if (level>stackLevel):
                newviewitem = qt.QListViewItem(self.lastAppendedStack[-1], item[0])
                self.lastAppendedStack.append(newviewitem)
            else:
                newviewitem = qt.QListViewItem(self.lastAppendedStack[level], self.lastAppendedStack[level+1], item[0])
                self.lastAppendedStack[level+1] = newviewitem
                for viewit in self.lastAppendedStack[max(self.maxLevel+2,level+2):]:
                    viewit.parent().setOpen(False)
                self.lastAppendedStack = self.lastAppendedStack[0:level+2]
            item = item + (newviewitem,)
            self.items.append(item)
            if (self.follow):
#                self.setCurrentItem(newviewitem)
                self.ensureItemVisible(newviewitem)
        self.buffer=[]

class MainWindow(qt.QMainWindow):

    def __init__(self, app, hit, timeout, autoAppend, follow, *args):
        apply(qt.QMainWindow.__init__, (self,) + args)
        self.app = app
        self.hit = hit
        self.timeout = timeout
        self.autoAppend = autoAppend

        self.content = qt.QFrame(self)
        self.setCentralWidget(self.content)
        self.contentlayout = qt.QVBoxLayout(self.content)
        self.controlBoxLayout = qt.QHBoxLayout(self.contentlayout)
        self.controlBoxLayout.setSpacing(20)
        self.controlBoxLayout.setMargin(10)

        self.maxLevelControl = qt.QSpinBox(0,10,1,self.content)
        self.followControl = qt.QCheckBox ('follow', self.content)
        self.autoAppendControl = qt.QCheckBox ('auto append', self.content)
        self.hview = HListView(follow, self.content)

        self.followControl.setOn(self.hview.follow)
        self.autoAppendControl.setOn(self.autoAppend)

        self.controlBoxLayout.addWidget(self.maxLevelControl)
        self.controlBoxLayout.addWidget(self.followControl)
        self.controlBoxLayout.addWidget(self.autoAppendControl)
        self.contentlayout.addWidget(self.hview)
        self.controlBoxLayout.insertStretch(-1)

        self.connect(self.maxLevelControl, qt.SIGNAL('valueChanged(int)'), self.hview.setMaxLevel)
        self.connect(self.followControl, qt.SIGNAL('toggled(bool)'), self.hview.toggleFollow)
        self.connect(self.autoAppendControl, qt.SIGNAL('toggled(bool)'), self.toggleAutoAppend)

        self.readerThread = ItemReaderThread(self.hit,self.hview.queueItem)
        self.readerThread.setDaemon(True)
        self.readerThread.start()

        self.postProcessBufferThread = PeriodicActionThread(self.timeout,self.processQueue)
        self.postProcessBufferThread.setDaemon(True)
        self.postProcessBufferThread.start()

    def toggleAutoAppend(self, *args):
        if (len(args)>0):
            self.autoAppend = args[0]
        else:
            self.autoAppend = not(self.autoAppend)

    def processQueue(self):
        if (self.autoAppend):
            self.app.postEvent(self.hview, qt.QCustomEvent(qt.QEvent.User))



class ItemReaderThread (threading.Thread):
    def __init__(self,it,action):
        self.it = it
        self.action = action
        threading.Thread.__init__ (self)

    def run(self):
        try:
            for item in self.it:
                self.action(item)
        except:
#            print "Unexpected error:", sys.exc_info()[0]
            pass


class PeriodicActionThread (threading.Thread):
    def __init__(self, timeout, action):
        self.timeout = timeout
        self.action = action
        threading.Thread.__init__ (self)

    def run(self):
        try:
            while True:
                self.action()
                time.sleep(self.timeout)
        except:
#            print "Unexpected error:", sys.exc_info()[0]
            pass



def usage():
    print 'SYNOPSIS'
    print '    python hierarchicviewer.py [OPTIONS] FILE'
    print '    python hierarchicviewer.py [OPTIONS] -'
    print ''
    print 'DESCRIPTION'
    print '    If FILE was given view content of file in a hierarchic view.'
    print '    If - is given view standard input in a hierarchic view.'
    print '    Input should contain UP and DOWN indicators that indicate'
    print '    to switch the level in the output hierarchy correspondingly.'
    print ''
    print '    --help'
    print '        View this help text.'
    print ''
    print '    -u REGEX , --up=REGEX'
    print '        Lines matching REGEX are considered as indicator to switch'
    print '        up in the hierarchy. (default: \'^ *\\}\')'
    print ''
    print '    -d REGEX , --down=REGEX'
    print '        Lines matching REGEX are considered as indicator to switch'
    print '        down in the hierarchy. (default: \'^ *\\{\')'
    print ''
    print '    -h, --hideupdown'
    print '        Hide lines that contain UP or DOWN indicators in the output.'
    print ''
    print '    -n, --nofollow'
    print '        If supplied the view does not follow if new lines are appended.'
    print ''
    print '    -t TIMEOUT, --timeout=TIMEOUT'
    print '        The input buffer it cleared periodically and added to the'
    print '        hierarchic view after TIMEOUT seconds. (default: 0.1)'


def main(args):

    try:
        (opts, args) = getopt.getopt(sys.argv[1:], "t:u:d:hn", ["timeout=", "up=", "down=", "hideupdown", "nofollow", "noappend", "help"])
    except getopt.GetoptError, err:
        # print help information and exit:
        print str(err) # will print something like "option -a not recognized"
        print ''
        usage()
        sys.exit(2)

    for (o, a) in opts:
        if o in ("--help",):
            usage()
            return

    # timeout to clear buffer and view items (default: 0.1s)
    timeout = 0.1

    # reexp to switch level up (default: {)
    up = regexpMatchFunction('^ *\{')

    # reexp to switch level down (default: })
    down = regexpMatchFunction('^ *\}')

    # switch to hide lines matching up and down (default: False)
    hideupdown = False

    # switch to determine if new lines are auto appended (default: True)
    autoAppend = True

    # switch to determine if view follows appended lines (default: True)
    follow = True

    if (len(args)==1):
        if (args[0]=='-'):
            infile = sys.stdin
        else:
            try:
                infile = file(args[0], 'r')
            except IOError:
                print 'Could not open file:', args[0]
                print ''
                usage()
                sys.exit(2)
    else:
        print 'You did not supply a file or the - argument.'
        print ''
        usage()
        sys.exit(2)

    for (o, a) in opts:
        if o in ("-t", "--timeout"):
            timeout = float(a)
        elif o in ("-u", "--up"):
            up = regexpMatchFunction(a)
        elif o in ("-d", "--down"):
            up = regexpMatchFunction(a)
        elif o in ("-h", "--hideupdown"):
            hideupdown = True
        elif o in ("-n", "--nofollow"):
            follow = False
        elif o in ("--noappend"):
            autoAppend = False

    it = FileIterator(infile)
    hit = HierarchyIterator(it, up, down, hideupdown)

    app=qt.QApplication(args)
    win=MainWindow(app, hit, timeout, autoAppend, follow)
    win.resize(1000,600)
    win.show()
    app.connect(app, qt.SIGNAL("lastWindowClosed()"),
                app, qt.SLOT("quit()"))

    app.exec_loop()

    infile.close()


if __name__=="__main__":
    main(sys.argv)

